// ==UserScript==

// @version     2.0.0
// @name        icefilms-movie-list-enhanced
// @description Show additional info from imdb on the good movie lists @ icefilms.info/movies
// @namespace   anton
// @author      Anton

// @resource    main css/main.css

// Libraries:   https://developers.google.com/speed/libraries/
// @require     https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
// @require     js/GM_fetch.min.js
// @require     js/functions.js
// @require     js/main.js

// @match       *.icefilms.info/*

// @grant       GM_getResourceText
// @grant       GM_xmlhttpRequest

// @downloadURL https://bitbucket.org/antonolsson91/icefilms-movie-list-enhanced/raw/master/userscript.user.js
// @updateURL   https://bitbucket.org/antonolsson91/icefilms-movie-list-enhanced/raw/master/userscript.user.js

// ==/UserScript==