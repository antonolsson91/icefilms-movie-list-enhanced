Storage.prototype.setObj = function(key, obj) {
    return this.setItem( key, JSON.stringify(obj) )
}
Storage.prototype.getObj = function(key) {
  var ret;

  try{
    ret = JSON.parse( this.getItem(key) )
  }catch(e){
    ret = false;
  }finally{
    return ret;
  }
}
Object.size = function(){
  var count = 0;
  $.each(this, function(key, value){
    count++;
  })
  return count;
};

function addImdbData(obj, link){
  var starWidth = parseFloat(obj.rating.trim().replace(',','.'))*20;
  $(`<span class="imdb_rating">Rating: ${obj.rating} / 10 [${obj.ratingCount}]</span> <div class="r1"><div style="width:${starWidth}px;" class="r2"><img src="/images/10stars.png"></div></div>`).insertAfter( link )
  $(`<span class="imdb_genres">Genres: ${ expandGenres(obj.genres) }</span>`).insertAfter( link )
}
function expandGenres(genres){
    var a = "";
    $.each(genres, function(index, genre){
      a += `<span class="${genre}">${genre}</span>`;
    });
    return a;
}

function getContent(){
  var movies = {}
  var temp = []
  var rowContent = []
  $.each( $('.list2 > *'), function(index, element){
    var outerHTML = $(element)[0].outerHTML;
    var value = $(element).html();
    var elementType = $(element).get(0).tagName;
    //console.log(outerHTML)
    if(elementType == "BR"){
      movies[temp].push(rowContent);
      rowContent = []
      return;
    }
    if(elementType == "H3"){
      movies[value] = [];
      temp = value;
    } else {
      rowContent.push(outerHTML);
    }


  } )
  return movies;
}