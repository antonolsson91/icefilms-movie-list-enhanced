(function($){
  $(document).ready(function(){
    $('head').prepend('<style>' + GM_getResourceText('main') + '</style>');

    if( !$('.edited').length ){
      $('.list').clone().toggleClass('list').toggleClass('list2').prependTo( $('.list').parent() );

      $('.list').addClass('edited').empty();
      $('.list2').hide();

      var dateCounter = 0;
      var rowCounter = 0;
      $.each( getContent(), function(date, rows){
        var dateContainer = $(`<div id="date-${dateCounter}"></div>`);
        dateCounter++;
        dateContainer.append(`<h3>${date}</h3>`);


        $.each( rows, function(index, row){
          var rowContainer = $(`<div id="row-${rowCounter}"></div>`);
          rowCounter++;
          rowContainer.append(row);
          dateContainer.append(rowContainer);
        });

        dateContainer.appendTo('.list');
      });
    }

    const key = 'icefilms=imdb'

    // SHIFT + F = remove data, reload
    $(document).on('keypress', function(e){
      if( e.shiftKey && e.which == 70){
        localStorage.removeItem(key)
        location.reload()
      }
    });

    // SHIFT + H = show data in console
    $(document).on('keypress', function(e){
      if( e.shiftKey && e.which == 72){
        console.log( Object.keys( localStorage.getObj(key) ).length, localStorage.getObj(key) );
      }
    });

    if( localStorage.getItem(key) == null || localStorage.getObj(key) == false || Array.isArray( localStorage.getObj(key) ) || typeof localStorage.getObj(key) != "object" ){
      localStorage.setObj( key, {} );
    }

    var doneCounter = 0;

    var array = localStorage.getObj(key);
    var arrayObj = localStorage.getItem(key);

    $('.list .imdb').each(function(i, e){
      var link = $(e).parent().find('.star');
      var url = $(e).attr('href').replace('http','https');

      var objExists = ( array[url] != null && typeof array[url] != "undefined" );
      if( !objExists ){
        (async function(){
          var fetchData = await GM_fetch( url )
          .then( response => response.text() )
          .then( function(res) {
            obj = {}
            obj.rating = $(res).find('span[itemprop="ratingValue"]').html()
            obj.ratingCount = $(res).find('span[itemprop="ratingCount"]').html()
            obj.genres = (function(){
              var a = [];
              $(res).find('span[itemprop="genre"]').each(function(i,e){
                a.push( $(e).html() );
              });
              return a;
            })()

            return obj;
          });

          var newArray = localStorage.getObj(key);
          newArray[url] = fetchData;
          localStorage.setObj(key, newArray);
          console.log( Object.keys( localStorage.getObj(key) ).length, localStorage.getObj(key) );
        })();
      }
      doneCounter++;
      addImdbData( localStorage.getObj(key)[url], link )

    });



    var L = $('.list .imdb').length;
    const activateCounter = false;
    if(activateCounter){
      var doneInterval = setInterval( function(){
        console.clear()
        console.log(`Progress: ${ ((doneCounter / L) * 100).toFixed(0) }% | ${doneCounter} / ${ L }`)
        if(doneCounter == 300)
        clearInterval(doneInterval)

      }, 1000 );
    }


  }); // .ready()
})(jQuery); // anonymous function